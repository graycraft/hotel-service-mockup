import Vue from 'vue'
import VueRouter from 'vue-router'

import HotelDetails from '@/views/HotelDetails'
import HotelList from '@/views/HotelList'

Vue.use(VueRouter)

export default new VueRouter({
  base: process.env.NODE_ENV === 'production'
    ? 'hotel-service-mockup'
    : process.env.BASE_URL,
  mode: 'history',
  routes: [
    {
      component: HotelDetails,
      name: 'hotel',
      path: '/hotel/:hotelId',
      props: true,
    },
    {
      component: HotelList,
      name: 'HotelList',
      path: '/'
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
