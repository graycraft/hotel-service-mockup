import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  actions: {
    fetchHotels ({ commit }) {
      Vue.axios.get(
        (process.env.NODE_ENV === 'production'
        ? '/hotel-service-mockup'
        : process.env.BASE_URL) + '/hotelsSample.json'
      ).then(res => {
        if (res.status === 200) {
          commit('updateHotels', res.data.hotels)
        }
      })
    }
  },
  getters: {
    getHotelById: state => hotelId => {
      return state.hotels.find(hotel => {
        return hotel.id === hotelId
      })
    }
  },
  mutations: {
    selectHotel: (state, hotelId) => {
      state.selectedHotelId = hotelId
    },
    updateHotels: (state, hotels) => state.hotels = hotels
  },
  plugins: [
    createPersistedState({ storage: window.localStorage })
  ],
  state: {
    hotels: [],
    selectedHotelId: ''
  },
  strict: process.env.NODE_ENV !== 'production'
})
