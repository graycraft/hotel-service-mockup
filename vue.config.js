module.exports = {
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/hotel-service-mockup/'
    : process.env.BASE_URL
}
